%%%-------------------------------------------------------------------
%% @doc knowhere top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(knowhere_sup).

-behaviour(supervisor).

-export([start_link/0]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%% sup_flags() = #{strategy => strategy(),         % optional
%%                 intensity => non_neg_integer(), % optional
%%                 period => pos_integer()}        % optional
%% child_spec() = #{id => child_id(),       % mandatory
%%                  start => mfargs(),      % mandatory
%%                  restart => restart(),   % optional
%%                  shutdown => shutdown(), % optional
%%                  type => worker(),       % optional
%%                  modules => modules()}   % optional
init([]) ->
    SupFlags = #{strategy => one_for_one,
                 intensity => 3,
                 period => 30},
    ChildSpecs = [#{id => knowhere_tivan, start => {knowhere_tivan, start_link, []}}
                 ,#{id => knowhere, start => {knowhere, start_link, []}}],
    {ok, {SupFlags, ChildSpecs}}.

%% internal functions
